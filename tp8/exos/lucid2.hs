{-# LANGUAGE OverloadedStrings #-}

import Lucid

myHtml :: Html ()
myHtml = do
    doctype
    html $ do
        head_ $ meta_ [charset_ "utf8"]
        body_ $ do
            h1_ "hello"
            img_ [src_ "toto.png"]
            p_ $ do
                "this is "
                a_ [href_ "toto.png"] "a link"

main :: IO()
main = renderToFile "out-lucid2.html" myHtml