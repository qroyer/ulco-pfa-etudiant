{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Message where

import Database.Selda
import Database.Selda.SQLite
import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)


data Message = Message
  { _message_id   :: Int
  , _message_content :: Text
  , _thread :: Int
  , _user :: Int
  } deriving (Generic, Show)

instance FromRow Message where
  fromRow = Message <$> field <*> field <*> field <*> field

instance SqlRow Message


dbSelectAllMessages :: Connection -> IO [Message]
dbSelectAllMessages conn =
  query_ conn "SELECT * FROM message"