let
  pkgs = import <nixpkgs> {};
  app-src = ./.. ;
  app = pkgs.haskellPackages.callCabal2nix "monprojet" ./.. {};

in
  pkgs.runCommand "app" { inherit app; } ''
    mkdir -p $out/{bin,static}
    cp ${app}/bin/monprojet $out/bin/
    cp ${app-src}/static/* $out/static/
  ''