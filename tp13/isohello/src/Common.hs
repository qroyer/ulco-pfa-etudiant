{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Common where

import Data.Proxy (Proxy(..))
import Miso
import Miso.String (ms, MisoString)
import Network.URI (URI)
import Servant.API
import Servant.Links

----------------------------------------------------------------------
-- model 
----------------------------------------------------------------------

data Model = Model
    { _msg :: MisoString
    } deriving (Eq)

initModel :: Model
initModel = Model "initial model"

----------------------------------------------------------------------
-- action
----------------------------------------------------------------------

data Action
    = ActionNone
    deriving (Eq)

----------------------------------------------------------------------
-- view 
----------------------------------------------------------------------

homeView :: Model -> View Action
homeView m =
    div_ []
        [ h1_ [] [text "isohello"]
        , p_ [] [text (_msg m)]
        , p_ [] [a_ [href_ (uriToMs linkHello)] [text "hello"]]
        , p_ [] [a_ [href_ (uriToMs linMul2)] [text "mul2 21"]]
        ]

----------------------------------------------------------------------
-- client routes 
----------------------------------------------------------------------

type ClientRoutes = HomeRoute

type HomeRoute = View Action

homeRoute :: URI
homeRoute = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @HomeRoute)

----------------------------------------------------------------------
-- api
----------------------------------------------------------------------

type HelloApi = "hello" :>  Get '[JSON] MisoString

type Mul2Api = "mul2" :> Capture "x" Int :> Get '[JSON] Int

----------------------------------------------------------------------
-- client api
----------------------------------------------------------------------

type PublicApi
    =    HelloApi  
    :<|> Mul2Api

linkHello :: URI
linkHello = linkURI $ safeLink (Proxy @PublicApi) (Proxy @HelloApi)

linMul2 :: URI
linMul2 = linkURI $ safeLink (Proxy @PublicApi) (Proxy @Mul2Api) 21

uriToMs :: URI -> MisoString
uriToMs = ms . show

