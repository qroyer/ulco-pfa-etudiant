{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module View where

import Lucid
    


indexPage :: Html ()
indexPage = do
    doctype_
    html_ $ do
        head_ $ do 
            link_ $ [rel_ "stylesheet", href_ "styles.css"]
            --link_ $ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css", integrity_ "sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z", crossorigin_ "anonymous"] 
        body_ $ do
            ul_ $ do
                li_ $ a_ [href_ "/index"] "index"
                li_ $ a_ [href_ "/riders"] "riders"
                img_ [src_ "bob.png"]
