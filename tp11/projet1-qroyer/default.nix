{ pkgs ? import <nixpkgs> {} }:
let drv = pkgs.haskellPackages.callCabal2nix "projet1-qroyer" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv
