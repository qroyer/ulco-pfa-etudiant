{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Thread where

import Database.Selda
import Database.Selda.SQLite
import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)
    

data Thread = Thread
  { _thread_id   :: Int
  , _thread_name :: Text
  } deriving (Generic, Show)

instance FromRow Thread where
fromRow = Thread <$> field <*> field 

instance SqlRow Thread
  
  
dbSelectAllThreads :: Connection -> IO [Thread]
dbSelectAllThreads conn =
    query_ conn "SELECT * FROM Thread"