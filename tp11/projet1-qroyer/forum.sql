-- create database:
-- sqlite3 forum.db < forum.sql

DROP TABLE IF EXISTS Thread;
CREATE TABLE Thread (
  thread_id INTEGER PRIMARY KEY AUTOINCREMENT,
  thread_name TEXT
);

DROP TABLE IF EXISTS User;
CREATE TABLE User (
  user_id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_name TEXT
);

DROP TABLE IF EXISTS Message;
CREATE TABLE Message (
  message_id INTEGER PRIMARY KEY AUTOINCREMENT,
  message_content TEXT,
  thread_id INT,
  user_id INT,
  FOREIGN KEY(thread_id) REFERENCES Thread(thread_id),
  FOREIGN KEY(user_id) REFERENCES User(user_id)
);


INSERT INTO Thread VALUES(1, 'Vacances 2020-2021');
INSERT INTO Thread VALUES(2, 'Master Info nouveau programme');

INSERT INTO Message VALUES(1,'Pas de vacances cette année.',1,1);
INSERT INTO Message VALUES(2,'Youpie!',1,2);
INSERT INTO Message VALUES(3,'Tous les cours passent en Haskell',2,2);

INSERT INTO User VALUES(1, 'John doe');
INSERT INTO User VALUES(2, 'Toto');



