{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import qualified Data.Text.Lazy as T
import Database.SQLite.Simple
import Lucid
import Text.Show
import Web.Scotty
import Text.Show
import Message
import Thread

main :: IO()
main = scotty 3000 $ do
    get "/" $ html $ renderText $ viewHomePage 
    get "/allthread" $ do
        threads <- liftIO $ withConnection "forum.db" Thread.dbSelectAllThreads
        html $ renderText $ viewPageAllthreads threads
    get "/alldata" $ do
        mess <- liftIO $ withConnection "forum.db" Message.dbSelectAllMessages
        html $ renderText $ viewPageAllMessages mess
    

viewHomePage :: Html()
viewHomePage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ulcoforum"
        body_ $ do
            h1_ "ULCOFORUM"
            ul_ $ do
                li_ $ a_ [href_ "/alldata"] "alldata"
                li_ $ a_ [href_ "/allthread"] "allthread"

viewPageAllMessages :: [Message] -> Html ()
viewPageAllMessages mess = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ulcoforum"
        body_ $ do
            h1_ "ULCOFORUM"
            ul_ $ do
                li_ $ a_ [href_ "/alldata"] "alldata"
                li_ $ a_ [href_ "/allthread"] "allthread"
            mapM_ viewAllMessages mess

viewAllMessages :: Message -> Html ()
viewAllMessages (Message id content person thread) = li_ $ do
    strong_  $ toHtml $ show person
    " : "
    toHtml  content


viewPageAllthreads :: [Thread] -> Html ()
viewPageAllthreads threads = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ulcoforum"
        body_ $ do
            h1_ "ULCOFORUM"
            ul_ $ do
                li_ $ a_ [href_ "/alldata"] "alldata"
                li_ $ a_ [href_ "/allthread"] "allthread"
            mapM_ viewAllThreads threads

viewAllThreads :: Thread -> Html ()
viewAllThreads (Thread id name) = li_ $ a_ [href_ "/"] $toHtml  name
