{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Lucid
import Web.Scotty as WB
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import View
import Model

main :: IO ()
main = scotty 3000 $ do
    middleware $ staticPolicy $ addBase "static"
    middleware $ gzip def { gzipFiles = GzipCompress }  
    get "/" $ html $ renderText indexPage
    get "/riders" $ WB.json (riders::[Rider])